<?php
if(isset($_GET["lang"]))
{
    if($_GET["lang"]==="ch")
    {
        include "Content_scripts/content_travel_chinese.php";
    }
    else{
        include "Content_scripts/content_travel.php";        
    }
}
else{
include "Content_scripts/content_travel.php";
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="keywords" content="Artificial intelligence, ">
        <title>TapReason</title>    
        <!-- Plugins CSS -->
        <link href="css/plugins/plugins.css" rel="stylesheet">
        <link href="linearicons/fonts.css" rel="stylesheet">
        <link href="css/style-saas-landing.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
        <link href="css/travel.css" rel="stylesheet">
        <link rel="icon" href="./images/tabicon.png">
    </head>

<body data-spy="scroll" data-target="#landingkit-navbar" data-offset="75">
        <div id="preloader">
            <div id="preloader-inner"></div>
        </div><!--/preloader-->

        <nav class="navbar navbar-expand-lg navbar-light bg-white navbar-transparent-light navbar-sticky">
            <div class="container">
                <a class="navbar-brand" href="./index.php">
                    <img src="images/logo3.png" alt="" class='logo-scroll'>
                    <img src="images/logo1.png" alt="" class='logo-trans'>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#landingkit-navbar" aria-controls="landingkit-navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="landingkit-navbar">
                    <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                    <span class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <a data-scroll ><?php echo $nav_items[0]; ?></a>
                                    </span>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="./product.php#features">Underlying magic</a>
                                        <a class="dropdown-item" hhref="./product.php#moment_scoring">Moment Scoring in Real-Time</a>
                                        <a class="dropdown-item" href="./product.php#dynamic">Dynamic Moment Scoring</a>
                                    </div>
                            </li>

                            <li class="nav-item dropdown">
                                    <span class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <a  data-scroll ><?php echo $nav_items[1]; ?></a>
                                    </span>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="./travel.php">e-Commerce Solutions</a>
                                        <a class="dropdown-item" href="./travel.php">travel Solutions</a>
                                        <a class="dropdown-item" href="./travel.php">Travel Solutions</a>
                                    </div>
                            </li> 
                            
                            <li class="nav-item dropdown">
                                    <span class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <a data-scroll ><?php echo $nav_items[2]; ?></a>
                                    </span>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="./company.php#mission">Our mission</a>
                                        <a class="dropdown-item" href="./company.php#team">Our team</a>
                                        <a class="dropdown-item" href="./company.php#reviews">Awards and certifications</a>
                                        
                                    </div>
                            </li>  

                            <li class="nav-item dropdown">
                                    <span class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <a data-scroll ><?php echo $nav_items[3]; ?></a>
                                    </span>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="./resources.php">Resource 1</a>
                                    
                                    </div>
                            </li> 
                                 
                        <li class="nav-item dropdown">
                            <span class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php echo $language; ?>
                            </span>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?php echo $language_link; ?>"><?php echo $language_alt; ?></a>   
                            </div>
                        </li>
                        <li class="nav-item">
                            <p>
                            	<a class="btn btn-white-outline modal-video my-3 btn-gray" onclick="Calendly.showPopupWidget('https://calendly.com/tapreason');return false;" role="button">SCHEDULE DEMO
                            	</a>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="fullScreen-particles bg-primary-dark background-color-primary">
          
          <div class="content-table">
             <div class="content-middle">
         <div class="container">
             <div class="row align-items-center">
                 <div class="col-lg-8 text-center mr-auto ml-auto">
                     <h3 class="mb20 h1 font300 pt100 text-white"><?php echo $travel_header_title;?></h3>
                     <p class="mb30 lead text-white-gray">
                     	<?php echo $travel_header_subtitle;?>
                     </p>
                     <div>
                         <a data-scroll href="#features" class="btn btn-primary mr-3 my-3"><?php echo $travel_header_button_title;?></a>
                         
                      
                     </div>
                 </div>
             </div>
         </div>
             </div>
          </div>
     </div><!--hero-->

     <main class="main-content">
        <div class="bg-faded text-dark pt90 pb90">
                <div class="container full">
                    <div class="row">
                        <div class="col-md-6">
                            <h6 class='section-subtitle'><?php echo $travel_main1_subtitle;?></h6>
                            <h2 class=''><?php echo $travel_main1_title;?></h2>
                            <p class="">
                                <?php echo $travel_main1_description;?>
                            </p>
                        </div>
                        <div class="col-md-6 center">
                            <?php echo $travel_main1_video;?>
                        </div>
                    </div>
                </div>
            </div><!--feature-img-->
        <section id='reviews' class='pt90 pb90'>
                    <div class="container">
    
                        <h4 class="text-dark-gray text-center">Trusted By Leading Companies</h4>
                        
                        <div class="row wow fadeInDown" data-wow-delay=".1s">
                            <div class="col owl-carousel owl-clients owl-theme">
                                <div class="item">
                                    <img src="images/adeco-logo.png" alt="">
                                </div>
                                <div class="item">
                                    <img src="images/dangdang-com-logo.png" alt="">
                                </div>
                                <div class="item">
                                    <img src="images/intuit-logo.png" alt="">
                                </div>
                                <div class="item">
                                    <img src="images/isracard-logo.png" alt="">
                                </div>
                                <div class="item">
                                    <img src="images/Lifesum-logo.png" alt="">
                                </div>
                                <div class="item">
                                    <img src="images/medisafe-logo.png" alt="">
                                </div>
                                <div class="item">
                                    <img src="images/prosperdaily-logo.png" alt="">
                                </div>
                                <div class="item vip-com-logo">
                                    <img src="images/vip-com-logo.png" alt="">
                                </div>
                                <div class="item zap-logo">
                                    <img src="images/zap-logo.jpg" alt="">
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


     </main><!--main-content-->
     <div  class="bg-parallax pt100 pb100" data-jarallax='{"speed": 0.2}' style='background-image: url("images/bg2.jpg")'>
        <div class='parallax-overlay'></div>
        <div class="container">
             <div class="row">
              	<div class="col-md-8 mr-auto ml-auto text-center pt100 pb100">
                  	<h2 class="text-white font300 h1 mb40">You're in good company</h2>
                <p>
                	<a class='btn btn-lg btn-primary' onclick="Calendly.showPopupWidget('https://calendly.com/tapreason');return false;" role="button">SCHEDULE DEMO
                	</a>
                </p>
            </div>
            </div>
        </div>
     </div>  
        
     <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="social-buttons col-lg-3">
                    <h5 class="">Follow Us</h5>
                    <span>
                        <a href="https://www.facebook.com/TapReason/" target="_blank" class="social-icon si-dark si-facebook si-dark-round">
                            <i class="fa fa-facebook"></i>
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="https://twitter.com/tapreason?lang=de" target="_blank" class="social-icon si-dark si-twitter si-dark-round">
                            <i class="fa fa-twitter"></i>
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="https://www.instagram.com/tapreason_/" target="_blank" class="social-icon si-dark si-instagram si-dark-round">
                            <i class="fa fa-instagram"></i>
                            <i class="fa fa-instagram"></i>
                        </a>
                    </span>
                </div>
            </div><hr class="mb70">
            <div class="row">
                <div class="col-md-4 mb30">
                    <h5 class="">About us</h5>
                    <a><?php echo $about_us;?>
                    </a>
                </div>
                <div class="col-md-4 mb30">
                    <h5 class="">Offices</h5>
                    <a>US: 85 Broad St. New York, NY 10004.</a> <br>
                    <a>CN: 588 Yan'an East Road Huangpu District Shanghai 31.</a> <br>
                    <a>IL: HaMada 1 St, Rehovot.</a><br>
                </div>
                <div class="col-md-4 mb30">
                    <h5 class="">Contact us</h5>
                    <a>Call us: +01123-4567-089</a><br>
                    <a>Mail us: hello [at] tapreason.com </a><br>
                    <a></a>
                </div>
            </div>                
            <div class="row">
                <div class="col-md-4 mb30">
                    <h5>Product</h5>
                    <a href="./product.php#features">Underlying Magic</a><br>
                    <a href="./product.php#moment_scoring">Moment Scoring in Real-Time</a><br>
                    <a href="./product.php#dynamic">Dynamic Moment Scoring</a><br>
                </div>
                <div class="col-md-4 mb30">
                    <h5>Customers</h5>
                    <a href="./e-commerce.php">E-Commerce Solutions</a><br>
                    <a href="./travel.php">travel Solutions</a><br>
                    <a href="./travel.php">Travel Solutions</a><br>
                </div>
                <div class="col-md-4 mb30">
                    <h5>Company</h5>
                    <a href="./company.php#mission">Our mission</a><br>
                    <a href="./company.php#team">Our team</a><br>
                    <a href="./company.php#reviews">Awards and certifications</a><br>
                </div>
            </div>
                
            </div><hr class="mb60">
            <div class="text-center copyright">
                <span>&copy; Copyright 2018. <a href="./index.php">TapReason</a> All rights reserved.</span>
            </div>
        </div>
    </footer>

    <!-- Calendly link widget begin -->
	<link href="https://assets.calendly.com/assets/external/widget.css" rel="stylesheet">
	<script src="https://assets.calendly.com/assets/external/widget.js" type="text/javascript"></script>
	<!-- Calendly link widget end -->

     <!--back to top-->
     <a href="#" class="back-to-top" id="back-to-top"><i class="icon-chevron-up"></i></a>
     <!-- jQuery first, then Tether, then Bootstrap JS. -->
     <script type="text/javascript" src="js/plugins/plugins.js"></script>
     <!--tweet-scroller-->
     <script src="tweetie/tweetie.min.js" type="text/javascript"></script>
     <script type="text/javascript" src="js/saas.products.custom.js"></script>
     <script src="js/jquery.particleground.min.js"></script>
     <script type="text/javascript" src="js/landing.custom.js"></script> 
     <script>
         //particle
 $('#home').particleground({
     dotColor: '#65749f',
     lineColor: '#65749f'
 });
         </script>
 	</body>
</html>

    
        