<?php
$servername = 'localhost';
$username = 'root';
$passowrd= '';
$nav_items= array();
$i=0;
$con = mysqli_connect($servername,$username,$passowrd,'tapreason');
if($con){
   $query = 'Select * from homepage';
   $query_run = mysqli_query($con, $query);
   if($query_run){
       while($data = mysqli_fetch_assoc($query_run))
       {
           $nav_items[0] =  $data["nav_item_1"];
           $nav_items[1] =  $data["nav_item_2"];
           $nav_items[2] =  $data["nav_item_3"];
           $nav_items[3] =  $data["nav_item_4"];
           $main_header = $data["main_header"];
           $sub_title = $data["subtitle"];
           $learn_more  = $data["button"];
       }
   }
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="keywords" content="Artificial intelligence, ">
        <title>Website TapReason</title>    
        <!-- Plugins CSS -->
        <link href="css/plugins/plugins.css" rel="stylesheet">
        <link href="linearicons/fonts.css" rel="stylesheet">
        <link href="css/style-saas-landing.css" rel="stylesheet">
        <link href="css/home.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
    </head>

    <body data-spy="scroll" data-target="#landingkit-navbar" data-offset="75">
        <div id="preloader">
            <div id="preloader-inner"></div>
        </div><!--/preloader-->

        <nav class="navbar navbar-expand-lg navbar-light bg-white navbar-transparent-light navbar-sticky">
            <div class="container">
                <a class="navbar-brand" href="./home.html">
                    <img src="images/logo3.png" alt="" class='logo-scroll'>
                    <img src="images/logo1.png" alt="" class='logo-trans'>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#landingkit-navbar" aria-controls="landingkit-navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="landingkit-navbar">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link active" data-scroll href="./product.html"><?php echo $nav_items[0]; ?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-scroll href=""><?php echo $nav_items[1]; ?></a>
                        </li>           
                       
                        <li class="nav-item">
                            <a class="nav-link" data-scroll href=""><?php echo $nav_items[2]; ?></a>
                        </li>
                         <li class="nav-item">
                            <a class="nav-link" data-scroll href=""><?php echo $nav_items[3]; ?></a>
                        </li>
                        <li class="nav-item dropdown">
                            <span class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                EN
                            </span>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                 <a class="dropdown-item" href="index.php?lang=ch">Chinese</a>
                                <a class="dropdown-item" href="index.php">English</a>
                                
                            </div>
                        </li>
                        <li class="nav-item">
                                <a href="https://wrapbootstrap.com/theme/assan-multipurpose-18-themes-admin-WB05F069P/?ref=wb_rakesh" target="_blank" class="btn btn-white-outline modal-video my-3">SCHEDULE DEMO</a> 
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div id="home" class="fullScreen-particles bg-primary-dark">
          
             <div class="content-table">
                <div class="content-middle">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-8 text-center mr-auto ml-auto">
                        <h3 class="mb20 h1 font300 pt100 text-white"><?php echo $main_header;?></h3>
                        <p class=" mb30 lead text-white-gray">
                                <?php echo $sub_title ?>
                        </p>
                        <div>
                            <a data-scroll href="#features" class="btn btn-primary mr-3 my-3"><?php echo $learn_more;?></a>
                            
                         
                        </div>
                    </div>
                </div>
            </div>
                </div>
             </div>
        </div><!--hero-->

        <main class="main-content">
            <section id="features" class='gradient-faded'>
                <div class="container pt90 pb90">
                    <h6 class="text-center section-subtitle"></h6>
                    <h3 class="text-center section-title mb70"><?php echo $main_title;?></h3>
                    <div class="row">
                        <div class="col-lg-4 border border-faded border-top-0 border-left-0 feature-icon text-center">
                            <i class="icon-magnifier text-primary"></i>
                            <h4>Analyze</h4>
                            <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since.
                            </p>
                        </div>
                        <div class="col-lg-4 feature-icon border border-faded border-left-0 border-top-0 text-center">
                            <i class="icon-cable text-success"></i>
                            <h4>Connect</h4>
                            <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            </p>
                        </div>
                        <div class="col-lg-4 feature-icon border border-faded border-left-0 border-top-0 border-right-0 text-center">
                            <i class="icon-coin-dollar text-danger"></i>
                            <h4>Monetize</h4>
                            <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard.
                            </p>
                        </div>
                    </div>
                </div>
            </section><!--features-->
            <div class="bg-primary-dark feature-img-section pt90 pb90">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <h6 class='section-subtitle text-white'>First define yours goals</h6>
                            <h2 class='text-white'>Find characteristics in user activity</h2>
                            <p class="text-white-gray">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets
                            </p>
                            
                        </div>
                        <div class="col-lg-6 center">
                                <div id="user-value">
                                        <div class='icon-container' id='icon-top-left'>
                                            <i class="icon-register"></i>
                                            <span>Register</span>
                                        </div>
                                        <div class='icon-container' id='icon-top-right'>
                                            <i class="icon-cash-dollar"></i>
                                            <span>Buy</span>
                                        </div>
                                        <div class='icon-container' id='icon-left-top'>
                                            <i class="icon-cloud-download"></i>
                                            <span>Download</span>
                                        </div>
                                        <div class='icon-container' id='icon-left-bottom'>
                                            <i class="icon-bullhorn"></i>
                                            <span>Share</span>
                                        </div>
                                        <div class='icon-container' id='icon-bottom-left'>
                                            <i class="icon-envelope"></i>
                                            <span>Suscribe</span>
                                        </div>
                                        <div class='icon-container' id='icon-bottom-right'>
                                            <i class="icon-star"></i>
                                            <span>Review</span>
                                        </div>
                                        <div class='icon-container' id='icon-right-top'>
                                            <i class="icon-bubble-dots"></i>
                                            <span>Comment</span>
                                        </div>
                                        <div class='icon-container' id='icon-right-bottom'>
                                            <i class="icon-tag"></i>
                                            <span>Reedem Coupon</span>
                                        </div>
                                    </div>
                        </div>

                    </div>
                </div>
            </div><!--feature-img-->
            <div class="bg-white pt90 pb50">
                <div class="container full-width">
                    <div class="row align-items-center">
                        <div class='col-lg-6 pb40 center'>
                            <div id="user-value-text">
                                    <div class='icon-container' id='icon-text-top-left'>
                                        <i class="icon-circle"></i>
                                        <span class="number">6.9%</span>
                                        <span>Register</span>
                                    </div>
                                    <div class='icon-container' id='icon-text-top-right'>
                                        <i class="icon-circle"></i>
                                        <span class="number">0.7%</span>
                                        <span>Buy</span>
                                    </div>
                                    <div class='icon-container' id='icon-text-left-top'>
                                        <i class="icon-circle"></i>
                                        <span class="number">5.4%</span>
                                        <span>Download</span>
                                    </div>
                                    <div class='icon-container' id='icon-text-left-bottom'>
                                        <i class="icon-circle"></i>
                                        <span class="number">3.7%</span>
                                        <span>Share</span>
                                    </div>
                                    <div class='icon-container' id='icon-text-bottom-left'>
                                        <i class="icon-circle"></i>
                                        <span class="number">6.2%</span>
                                        <span>Subscribe</span>
                                    </div>
                                    <div class='icon-container' id='icon-text-bottom-right'>
                                        <i class="icon-circle"></i>
                                        <span class="number">11%</span>
                                        <span>Review</span>
                                    </div>
                                    <div class='icon-container' id='icon-text-right-top'>
                                        <i class="icon-circle"></i>
                                        <span class="number">9.3%</span>
                                        <span>Comment</span>
                                    </div> 
                                    <div class='icon-container' id='icon-text-right-bottom'>
                                        <i class="icon-circle"></i>
                                        <span class="number">16%</span>
                                        <span>Reedem Coupon</span>
                                    </div>
                                </div>
                        </div>
                        <div class="col-lg-5 ml-auto pb40">
                            <h6 class='section-subtitle'>Second step is predict the expected engagement in Real-time.</h6>
                            <h2>Real-Time prediction</h2>
                            <p>
                                    Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.
                            </p>
                            
                        </div>

                    </div>
                </div>
            </div><!--feature-img-->
            <div class="bg-faded pt90 pb50">
                <div class="container">
                    <div class="row align-items-center">

                        <div class="col-lg-5 pb40">
                            <h6 class='section-subtitle'>Finally triggering next best action</h6>
                            <h2>User engagement metric</h2>
                            <p>
                                    It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.
                            </p>
                        </div>
                        <div class='col-lg-6 ml-auto pb40 center'>
                                <div id="user-value-two">
                                        <div class='icon-container' id='icon-text-top-left'>
                                            <i class="icon-circle"></i>
                                            <span class="number">6.9%</span>
                                            <span>Register</span>
                                        </div>
                                        <div class='icon-container' id='icon-text-top-right'>
                                            <i class="icon-circle"></i>
                                            <span class="number">0.7%</span>
                                            <span>Buy</span>
                                        </div>
                                        <div class='icon-container' id='icon-text-left-top'>
                                            <i class="icon-circle"></i>
                                            <span class="number">5.4%</span>
                                            <span>Download</span>
                                        </div>
                                        <div class='icon-container' id='icon-text-left-bottom'>
                                            <i class="icon-circle"></i>
                                            <span class="number">3.7%</span>
                                            <span>Share</span>
                                        </div>
                                        <div class='icon-container' id='icon-text-bottom-left'>
                                            <i class="icon-circle"></i>
                                            <span class="number">6.2%</span>
                                            <span>Subscribe</span>
                                        </div>
                                        <div class='icon-container' id='icon-text-bottom-right'>
                                            <i class="icon-circle"></i>
                                            <span class="number">11%</span>
                                            <span>Review</span>
                                        </div>
                                        <div class='icon-container' id='icon-text-right-top'>
                                            <i class="icon-circle"></i>
                                            <span class="number">9.3%</span>
                                            <span>Comment</span>
                                        </div> 
                                        <div class='icon-container' id='icon-text-right-bottom'>
                                            <i class="icon-circle"></i>
                                            <span class="number">16%</span>
                                            <span>Reedem Coupon</span>
                                        </div>
                                    </div>
                        </div>
                    </div>
                </div>
            </div><!--feature-img-->
            <section id='reviews' class='pt90 pb90'>
                    <div class="container">
    
                        <h4 class="text-dark-gray text-center">Trusted by landing companies</h4>
                        
    
                        <div class="row wow fadeInDown" data-wow-delay=".1s">
                            <div class="col owl-carousel owl-clients owl-theme">
                                <div class="item">
                                    <img src="images/adeco-logo.png" alt="">
                                </div>
                                <div class="item">
                                    <img src="images/dangdang-com-logo.png" alt="">
                                </div>
                                <div class="item">
                                    <img src="images/intuit-logo.png" alt="">
                                </div>
                                <div class="item">
                                    <img src="images/isracard-logo.png" alt="">
                                </div>
                                <div class="item">
                                    <img src="images/Lifesum-logo.png" alt="">
                                </div>
                                <div class="item">
                                    <img src="images/medisafe-logo.png" alt="">
                                </div>
                                <div class="item">
                                    <img src="images/prosperdaily-logo.png" alt="">
                                </div>
                                <div class="item vip-com-logo">
                                    <img src="images/vip-com-logo.png" alt="">
                                </div>
                                <div class="item zap-logo">
                                    <img src="images/zap-logo.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img src="images/intuit-logo.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>


        </main><!--main-content-->
        <div  class="bg-parallax pt100 pb100" data-jarallax='{"speed": 0.2}' style='background-image: url("images/bg2.jpg")'>
            <div class='parallax-overlay'></div>
            <div class="container">
                <div class="row">
                     <div class="col-md-8 mr-auto ml-auto text-center pt100 pb100">
                         <h2 class="text-white font300 h1 mb40">You're in good company</h2>
                         <a href='#' class='btn btn-lg btn-primary'>SCHEDULE DEMO</a>
                </div>
                </div>
            </div>
        </div>  
           
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="social-buttons col-lg-3 mb30">
                        <h5 class=" mb20">Follow Us</h5>
                        <span>
                            <a href="#" class="social-icon si-dark si-facebook si-dark-round">
                                <i class="fa fa-facebook"></i>
                                <i class="fa fa-facebook"></i>
                            </a>
                            <a href="#" class="social-icon si-dark si-twitter si-dark-round">
                                <i class="fa fa-twitter"></i>
                                <i class="fa fa-twitter"></i>
                            </a>
                            <a href="#" class="social-icon si-dark si-g-plus si-dark-round">
                                <i class="fa fa-google-plus"></i>
                                <i class="fa fa-google-plus"></i>
                            </a>
                            <a href="#" class="social-icon si-dark si-skype si-dark-round">
                                <i class="fa fa-skype"></i>
                                <i class="fa fa-skype"></i>
                            </a>
                        </span>
                    </div>
                    <div class='col-lg-3'>
                        <h5 class=" mb20">Call Us</h5> 
                        <p class='lead'>+01 123-4567-089</p>
                    </div>
                     <div class='col-lg-3'>
                        <h5 class=" mb20">Mail Us</h5> 
                        <p class='lead'>support@assan.com</p>
                    </div>
                     <div class='col-lg-3'>
                        <h5 class=" mb20">We're Hiring</h5> 
                        <a class='btn btn-primary'>Apply Now</a>
                    </div>
                </div><hr class="mb70">
                <div class="row align-items-start">
                    <div class="col-lg-5 mb30">
                        <h5 class="mb20">About us</h5>
                        <ul class="list-unstyled">
                            <li>
                                <a href="#">About us</a>
                            </li>
                            <li>
                                <a href="#">News</a>
                            </li>
                            <li>
                                <a href="#">Terms & conditions</a>
                            </li>
                            <li>
                                <a href="#">Contact Us</a>
                            </li>
                            <li>
                                <a href="#">Bounties</a>
                            </li>
                            <li>
                                <a href="#">Privacy Policy</a> 
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 ml-auto mb30">
                        <h5 class="mb20">About us</h5>
                        <ul class="list-unstyled">
                            <li>
                                <a href="#">About us</a>
                            </li>
                            <li>
                                <a href="#">News</a>
                            </li>
                            <li>
                                <a href="#">Terms & conditions</a>
                            </li>
                            <li>
                                <a href="#">Contact Us</a>
                            </li>
                            <li>
                                <a href="#">Bounties</a>
                            </li>
                            <li>
                                <a href="#">Privacy Policy</a> 
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 mr-auto mb30">
                        <h5 class="mb20">More Demos</h5>
                        <ul class="list-unstyled">
                            <li>
                                <a href="#">Classic Template</a>
                            </li>
                            <li>
                                <a href="#">Personal Portfolio</a>
                            </li>
                            <li>
                                <a href="#">Restaurants</a>
                            </li>
                            <li>
                                <a href="#">Real Estate</a>
                            </li>
                            <li>
                                <a href="#">Resume & Cv</a>
                            </li>
                            <li>
                                <a href="#">App Landing</a> 
                            </li>
                        </ul>
                    </div>
                </div><hr class="mb60">
                <div class="text-center copyright">
                    <span>&copy; Copyright 2014-2017. Designed by <a href="#">Assan</a></span>
                </div>
            </div>
        </footer>
        <!--back to top-->
        <a href="#" class="back-to-top" id="back-to-top"><i class="icon-chevron-up"></i></a>
        <!-- jQuery first, then Tether, then Bootstrap JS. -->
        <script type="text/javascript" src="js/plugins/plugins.js"></script>
        <!--tweet-scroller-->
        <script src="tweetie/tweetie.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="js/saas.products.custom.js"></script>
        <script src="js/jquery.particleground.min.js"></script>
        <script type="text/javascript" src="js/landing.custom.js"></script> 
        <script>
            //particle
    $('#home').particleground({
        dotColor: '#65749f',
        lineColor: '#65749f'
    });
            </script>
    </body>
</html>
