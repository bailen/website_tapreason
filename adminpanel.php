<?php
session_start();
include "Content_scripts/content_index.php";
if(isset($_SESSION["admin"]))
{
?>
<html>
<head>
    <title>Admin Panel</title>
    <!-- Latest compiled and minified CSS -->
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
 
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 
 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
 
 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> 

 <!-- Font Awesome CDN -->
 <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="#">Tap Reason Admin Dashboard</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="#" class="btn btn-danger text-light" id="logout">Logout <i class="fa fa-power-off"></i></a>
      </li>    
    </ul> 
</nav>

<div class="container-fluid">
    <div class="alert"></div>
<div class="container">
  	<h4>Please select the version below to start editing -</h4>
  	<div id="accordion">
    	<div class="card">
		      	<div class="card-header">
		        	<a class="card-link" data-toggle="collapse" href="#collapseOne">
		          	Edit English Version
		        	</a>
		      	</div>
		    <div id="collapseOne" class="collapse" data-parent="#accordion">
	        	<div class="card-body">
	        		<table>
	        			<tr>
	        				<th>
	        					<a href="adminpanel_index.php">index.php English <i class="fa fa-arrow-circle-right"></i></a>
	        				</th>
	        			</tr>
		        		<tr>
		        			<th>
		        				<a href="adminpanel_product.php">product.php English <i class="fa fa-arrow-circle-right"></i></a>
		        			</th>
		        		</tr>
		        		<tr>
		        			<th>
		        				<a href="adminpanel_ecommerce.php">ecommerce.php English <i class="fa fa-arrow-circle-right"></i></a>
		        			</th>
		        		</tr>
		        		<tr>
		        			<th>
		        				<a href="adminpanel_apps.php">apps.php English <i class="fa fa-arrow-circle-right"></i></a>
		        			</th>
		        		</tr>
		        		<tr>
		        			<th>
		        				<a href="adminpanel_travel.php">travel.php English <i class="fa fa-arrow-circle-right"></i></a>
		        			</th>
		        		</tr>
		        		<tr>
		        			<th>
		        				<a href="adminpanel_company.php">company.php English <i class="fa fa-arrow-circle-right"></i></a>
		        			</th>
		        		</tr>
		        		<tr>
		        			<th>
		        				<a href="adminpanel_resources.php">resources.php English <i class="fa fa-arrow-circle-right"></i></a>
		        			</th>
		        		</tr>
		        	</table>
		        </div>
		    </div>
	    </div>
	    <div class="card">
				<div class="card-header">
			        <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
			       	Edit Chinese Version
			        </a>
				</div>
			    <div id="collapseTwo" class="collapse" data-parent="#accordion">
			        <div class="card-body">
			        <table>
	        			<tr>
	        				<th>
	        					<a href="adminpanel_index_chinese.php">index.php Chinese <i class="fa fa-arrow-circle-right"></i></a>
	        				</th>
	        			</tr>
		        		<tr>
		        			<th>
		        				<a href="adminpanel_product_chinese.php">product.php Chinese <i class="fa fa-arrow-circle-right"></i></a>
		        			</th>
		        		</tr>
		        		<tr>
		        			<th>
		        				<a href="adminpanel_ecommerce_chinese.php">ecommerce.php Chinese <i class="fa fa-arrow-circle-right"></i></a>
		        			</th>
		        		</tr>
		        		<tr>
		        			<th>
		        				<a href="adminpanel_apps_chinese.php">apps.php Chinese <i class="fa fa-arrow-circle-right"></i></a>
		        			</th>
		        		</tr>
		        		<tr>
		        			<th>
		        				<a href="adminpanel_travel_chinese.php">travel.php Chinese <i class="fa fa-arrow-circle-right"></i></a>
		        			</th>
		        		</tr>
		        		<tr>
		        			<th>
		        				<a href="adminpanel_company_chinese.php">company.php Chinese <i class="fa fa-arrow-circle-right"></i></a>
		        			</th>
		        		</tr>
		        		<tr>
		        			<th>
		        				<a href="adminpanel_resources_chinese.php">resources.php Chinese <i class="fa fa-arrow-circle-right"></i></a>
		        			</th>
		        		</tr>
		        	</table>
			        </div>
			    </div>
	    </div>

  </div>
</div>

</div>
   

    <!--Logout script-->
    <script>
    $(document).ready(function(){
        $("#logout").click(function(){
            window.location.assign("logout.php");
        });
    });
    </script>

    <!--Script to edit the items-->
    <script>
        $(document).ready(function(){
            $(".save").hide();
            $(".cancel").hide();
            $(".new-text").hide();
            $(".edit").click(function(e){
                e.preventDefault();
                $(this).siblings(".save").show();
                $(this).siblings(".cancel").show();
                $(this).addClass("disabled");
                $(this).siblings(".new-text").show();
                
           
            });
        });
    </script>

    <!--Script to cancel editing-->
    <script>
        $(document).ready(function(){
            $(".cancel").click(function(e){
                e.preventDefault();
               $(this).siblings(".new-text").hide();
               $(".edit").removeClass("disabled");
               $(this).siblings(".save").hide();
               $(this).hide();
            });
        });
    </script>

</body>
</html>
<?php
}
else{
    header('location:index.php');
}
?>