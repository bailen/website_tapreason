<?php

//Nav - Items
$myfile = fopen("Content_travel/nav-menu.txt", "r") or die("Unable to open file!");
$i=0;
$nav_items = array(5);
while(!feof($myfile)) {
    $nav_items[$i] = fgets($myfile);
    $i++;
  }
fclose($myfile);

//Language
$myfile = fopen("Content_travel/language.txt", "r") or die("Unable to open file!");
$language =  fread($myfile,filesize("Content_travel/language.txt"));
fclose($myfile);

//Alternative Language
$myfile = fopen("Content_travel/language-alt.txt", "r") or die("Unable to open file!");
$language_alt =  fread($myfile,filesize("Content_travel/language-alt.txt"));
fclose($myfile);

//Language Link
$myfile = fopen("Content_travel/language-link.txt", "r") or die("Unable to open file!");
$language_link =  fread($myfile,filesize("Content_travel/language-link.txt"));
fclose($myfile);

//HEADER
$myfile = fopen("Content_travel/travel-header-title.txt", "r") or die("Unable to open file!");
$travel_header_title =  fread($myfile,filesize("Content_travel/travel-header-title.txt"));
fclose($myfile);

//SUBTITLE
$myfile = fopen("Content_travel/travel-header-subtitle.txt", "r") or die("Unable to open file!");
$travel_header_subtitle =  fread($myfile,filesize("Content_travel/travel-header-subtitle.txt"));
fclose($myfile);

//Learn More Button
$myfile = fopen("Content_travel/travel-header-button-title.txt", "r") or die("Unable to open file!");
$travel_header_button_title =  fread($myfile,filesize("Content_travel/travel-header-button-title.txt"));
fclose($myfile);

//travel_main1_title
$myfile = fopen("Content_travel/travel-main1-title.txt", "r") or die("Unable to open file!");
$travel_main1_title = fread($myfile,filesize("Content_travel/travel-main1-title.txt"));
fclose($myfile);

//travel_main1_subtitle
$myfile = fopen("Content_travel/travel-main1-subtitle.txt", "r") or die("Unable to open file!");
$travel_main1_subtitle =  fread($myfile,filesize("Content_travel/travel-main1-subtitle.txt"));
fclose($myfile);

//travel_main1_description
$myfile = fopen("Content_travel/travel-main1-description.txt", "r") or die("Unable to open file!");
$travel_main1_description =  fread($myfile,filesize("Content_travel/travel-main1-description.txt"));
fclose($myfile);

//travel_main1_video
$myfile = fopen("Content_travel/travel-main1-video.txt", "r") or die("Unable to open file!");
$travel_main1_video =  fread($myfile,filesize("Content_travel/travel-main1-video.txt"));
fclose($myfile);

//about_us
$myfile = fopen("Content_travel/about-us.txt", "r") or die("Unable to open file!");
$about_us =  fread($myfile,filesize("Content_travel/about-us.txt"));
fclose($myfile);

?>