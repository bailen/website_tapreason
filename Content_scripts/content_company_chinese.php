<?php

//Nav - Items
$myfile = fopen("Content_company_chinese/nav-menu.txt", "r") or die("Unable to open file!");
$i=0;
$nav_items = array(5);
while(!feof($myfile)) {
    $nav_items[$i] = fgets($myfile);
    $i++;
  }
fclose($myfile);

//Language
$myfile = fopen("Content_company_chinese/language.txt", "r") or die("Unable to open file!");
$language =  fread($myfile,filesize("Content_company_chinese/language.txt"));
fclose($myfile);

//Alternative Language
$myfile = fopen("Content_company_chinese/language-alt.txt", "r") or die("Unable to open file!");
$language_alt =  fread($myfile,filesize("Content_company_chinese/language-alt.txt"));
fclose($myfile);

//Language Link
$myfile = fopen("Content_company_chinese/language-link.txt", "r") or die("Unable to open file!");
$language_link =  fread($myfile,filesize("Content_company_chinese/language-link.txt"));
fclose($myfile);

//Mission
$myfile = fopen("Content_company_chinese/mission-title.txt", "r") or die("Unable to open file!");
$mission_title =  fread($myfile,filesize("Content_company_chinese/mission-title.txt"));
fclose($myfile);

//Mission Subtitle
$myfile = fopen("Content_company_chinese/mission-subtitle.txt", "r") or die("Unable to open file!");
$mission_subtitle =  fread($myfile,filesize("Content_company_chinese/mission-subtitle.txt"));
fclose($myfile);

//Learn More Button
$myfile = fopen("Content_company_chinese/company-header-button-title.txt", "r") or die("Unable to open file!");
$company_header_button_title =  fread($myfile,filesize("Content_company_chinese/company-header-button-title.txt"));
fclose($myfile);

//about_us
$myfile = fopen("Content_company_chinese/about-us.txt", "r") or die("Unable to open file!");
$about_us =  fread($myfile,filesize("Content_company_chinese/about-us.txt"));
fclose($myfile);

?>