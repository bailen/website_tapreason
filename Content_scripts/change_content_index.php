<?php
if(isset($_POST["olddata"]) && isset($_POST["newdata"]) && isset($_POST["filename"]))
{
$f = $_POST["filename"];
$olddata = $_POST["olddata"];
$newdata = $_POST["newdata"];
$success_msg="Updates saved successfully, please wait for a second for changes to take place.";
if(strcmp(trim($f), trim("nav-menu.txt"))==0)
{
$fp = fopen("../Content_index/".$f, "r");
$fp1= fopen("../Content_index/sample_file.txt","w");
$data='';

while(!feof($fp)) {
    
     $data = fgets($fp);
     if(strcmp(trim($data), trim($olddata))==0){
        
        fwrite($fp1,$newdata.PHP_EOL);
        
     }
     else{
         fwrite($fp1,$data);
     }
  }
  fclose($fp);
  fclose($fp1);
  unlink("../Content_index/".$f);
  rename("../Content_index/sample_file.txt", "../Content_index/nav-menu.txt");
  echo $success_msg;
}

//Language
if(strcmp(trim($f), trim("language.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//Alternative Language
if(strcmp(trim($f), trim("language-alt.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//Language Link
if(strcmp(trim($f), trim("language-link.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//Header text
if(strcmp(trim($f), trim("homePage-header-title.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//Subtitle
if(strcmp(trim($f), trim("homePage-header-subtitle.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage-header-button-title
if(strcmp(trim($f), trim("homePage-header-button-title.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

///homePage-main1-title 
if(strcmp(trim($f), trim("/homePage-main1-title.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage_main1_subtitle 
if(strcmp(trim($f), trim("homePage_main1_subtitle.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage-main1-icon1-title
if(strcmp(trim($f), trim("homePage-main1-icon1-title.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage-main1-icon1-description
if(strcmp(trim($f), trim("homePage-main1-icon1-description.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage-main1-icon2-title
if(strcmp(trim($f), trim("homePage-main1-icon2-title.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage-main1-icon2-description
if(strcmp(trim($f), trim("homePage-main1-icon2-description.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage-main1-icon3-title
if(strcmp(trim($f), trim("homePage-main1-icon3-title.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage-main1-icon3-description
if(strcmp(trim($f), trim("homePage-main1-icon3-description.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage_main2_subtitle
if(strcmp(trim($f), trim("homePage-main2-subtitle.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage_main2_title
if(strcmp(trim($f), trim("homePage-main2-title.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage_main2_description
if(strcmp(trim($f), trim("homePage-main2-description.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage_main2_button_title
if(strcmp(trim($f), trim("homePage-main2-button-title.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage_main3_subtitle
if(strcmp(trim($f), trim("homePage-main3-subtitle.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage_main3_title
if(strcmp(trim($f), trim("homePage-main3-title.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage_main3_description
if(strcmp(trim($f), trim("homePage-main3-description.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage_main3_button_title
if(strcmp(trim($f), trim("homePage-main3-button-title.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage_main4_subtitle
if(strcmp(trim($f), trim("homePage-main4-subtitle.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage_main4_title
if(strcmp(trim($f), trim("homePage-main4-title.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage_main4_description
if(strcmp(trim($f), trim("homePage-main4-description.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//homePage_main4_button_title
if(strcmp(trim($f), trim("homePage-main4-button-title.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//about_us
if(strcmp(trim($f), trim("about-us.txt"))==0)
{
    $fp = fopen("../Content_index/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}


}
?>