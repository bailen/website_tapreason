<?php
if(isset($_POST["olddata"]) && isset($_POST["newdata"]) && isset($_POST["filename"]))
{
$f = $_POST["filename"];
$olddata = $_POST["olddata"];
$newdata = $_POST["newdata"];
$success_msg="Updates saved successfully, please wait for a second for changes to take place.";
if(strcmp(trim($f), trim("nav-menu.txt"))==0)
{
$fp = fopen("../Content_resources_chinese/".$f, "r");
$fp1= fopen("../Content_resources_chinese/sample_file.txt","w");
$data='';

while(!feof($fp)) {
    
     $data = fgets($fp);
     if(strcmp(trim($data), trim($olddata))==0){
        
        fwrite($fp1,$newdata.PHP_EOL);
        
     }
     else{
         fwrite($fp1,$data);
     }
  }
  fclose($fp);
  fclose($fp1);
  unlink("../Content_resources_chinese/".$f);
  rename("../Content_resources_chinese/sample_file.txt", "../Content_resources_chinese/nav-menu.txt");
  echo $success_msg;
}

//Language
if(strcmp(trim($f), trim("language.txt"))==0)
{
    $fp = fopen("../Content_resources_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//Alternative Language
if(strcmp(trim($f), trim("language-alt.txt"))==0)
{
    $fp = fopen("../Content_resources_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//Language Link
if(strcmp(trim($f), trim("language-link.txt"))==0)
{
    $fp = fopen("../Content_resources_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//resources Title
if(strcmp(trim($f), trim("resources-title.txt"))==0)
{
    $fp = fopen("../Content_resources_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//resources Subtitle
if(strcmp(trim($f), trim("resources-subtitle.txt"))==0)
{
    $fp = fopen("../Content_resources_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//resources-header-button-title
if(strcmp(trim($f), trim("resources-header-button-title.txt"))==0)
{
    $fp = fopen("../Content_resources_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//about_us
if(strcmp(trim($f), trim("about-us.txt"))==0)
{
    $fp = fopen("../Content_resources_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}


}
?>