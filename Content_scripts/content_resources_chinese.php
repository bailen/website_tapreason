<?php

//Nav - Items
$myfile = fopen("Content_resources_chinese/nav-menu.txt", "r") or die("Unable to open file!");
$i=0;
$nav_items = array(5);
while(!feof($myfile)) {
    $nav_items[$i] = fgets($myfile);
    $i++;
  }
fclose($myfile);

//Language
$myfile = fopen("Content_resources_chinese/language.txt", "r") or die("Unable to open file!");
$language =  fread($myfile,filesize("Content_resources_chinese/language.txt"));
fclose($myfile);

//Alternative Language
$myfile = fopen("Content_resources_chinese/language-alt.txt", "r") or die("Unable to open file!");
$language_alt =  fread($myfile,filesize("Content_resources_chinese/language-alt.txt"));
fclose($myfile);

//Language Link
$myfile = fopen("Content_resources_chinese/language-link.txt", "r") or die("Unable to open file!");
$language_link =  fread($myfile,filesize("Content_resources_chinese/language-link.txt"));
fclose($myfile);

//resources
$myfile = fopen("Content_resources_chinese/resources-title.txt", "r") or die("Unable to open file!");
$resources_title =  fread($myfile,filesize("Content_resources_chinese/resources-title.txt"));
fclose($myfile);

//resources Subtitle
$myfile = fopen("Content_resources_chinese/resources-subtitle.txt", "r") or die("Unable to open file!");
$resources_subtitle =  fread($myfile,filesize("Content_resources_chinese/resources-subtitle.txt"));
fclose($myfile);

//Learn More Button
$myfile = fopen("Content_resources_chinese/resources-header-button-title.txt", "r") or die("Unable to open file!");
$resources_header_button_title =  fread($myfile,filesize("Content_resources_chinese/resources-header-button-title.txt"));
fclose($myfile);

//about_us
$myfile = fopen("Content_resources_chinese/about-us.txt", "r") or die("Unable to open file!");
$about_us =  fread($myfile,filesize("Content_resources_chinese/about-us.txt"));
fclose($myfile);

?>