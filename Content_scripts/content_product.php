<?php

//Nav - Items
$myfile = fopen("Content_product/nav-menu.txt", "r") or die("Unable to open file!");
$i=0;
$nav_items = array(5);
while(!feof($myfile)) {
    $nav_items[$i] = fgets($myfile);
    $i++;
  }
fclose($myfile);

//Language
$myfile = fopen("Content_product/language.txt", "r") or die("Unable to open file!");
$language =  fread($myfile,filesize("Content_product/language.txt"));
fclose($myfile);

//Alternative Language
$myfile = fopen("Content_product/language-alt.txt", "r") or die("Unable to open file!");
$language_alt =  fread($myfile,filesize("Content_product/language-alt.txt"));
fclose($myfile);

//Language Link
$myfile = fopen("Content_product/language-link.txt", "r") or die("Unable to open file!");
$language_link =  fread($myfile,filesize("Content_product/language-link.txt"));
fclose($myfile);

//HEADER
$myfile = fopen("Content_product/product-header-title.txt", "r") or die("Unable to open file!");
$product_header_title =  fread($myfile,filesize("Content_product/product-header-title.txt"));
fclose($myfile);

//SUBTITLE
$myfile = fopen("Content_product/product-header-subtitle.txt", "r") or die("Unable to open file!");
$product_header_subtitle =  fread($myfile,filesize("Content_product/product-header-subtitle.txt"));
fclose($myfile);

//Learn More Button
$myfile = fopen("Content_product/product-header-button-title.txt", "r") or die("Unable to open file!");
$product_header_button_title =  fread($myfile,filesize("Content_product/product-header-button-title.txt"));
fclose($myfile);

//product_main1_title
$myfile = fopen("Content_product/product-main1-title.txt", "r") or die("Unable to open file!");
$product_main1_title = fread($myfile,filesize("Content_product/product-main1-title.txt"));
fclose($myfile);

//product_main1_subtitle
$myfile = fopen("Content_product/product-main1-subtitle.txt", "r") or die("Unable to open file!");
$product_main1_subtitle =  fread($myfile,filesize("Content_product/product-main1-subtitle.txt"));
fclose($myfile);

//product_main1_description
$myfile = fopen("Content_product/product-main1-description.txt", "r") or die("Unable to open file!");
$product_main1_description =  fread($myfile,filesize("Content_product/product-main1-description.txt"));
fclose($myfile);

//product_main2_title
$myfile = fopen("Content_product/product-main2-title.txt", "r") or die("Unable to open file!");
$product_main2_title =  fread($myfile,filesize("Content_product/product-main2-title.txt"));
fclose($myfile);

//product_main2_subtitle
$myfile = fopen("Content_product/product-main2-subtitle.txt", "r") or die("Unable to open file!");
$product_main2_subtitle =  fread($myfile,filesize("Content_product/product-main2-subtitle.txt"));
fclose($myfile);

//product_main2_description
$myfile = fopen("Content_product/product-main2-description.txt", "r") or die("Unable to open file!");
$product_main2_description =  fread($myfile,filesize("Content_product/product-main2-description.txt"));
fclose($myfile);

//product_main3_title
$myfile = fopen("Content_product/product-main3-title.txt", "r") or die("Unable to open file!");
$product_main3_title =  fread($myfile,filesize("Content_product/product-main3-title.txt"));
fclose($myfile);

//product_main3_subtitle
$myfile = fopen("Content_product/product-main3-subtitle.txt", "r") or die("Unable to open file!");
$product_main3_subtitle =  fread($myfile,filesize("Content_product/product-main3-subtitle.txt"));
fclose($myfile);

//product_main3_description
$myfile = fopen("Content_product/product-main3-description.txt", "r") or die("Unable to open file!");
$product_main3_description =  fread($myfile,filesize("Content_product/product-main3-description.txt"));
fclose($myfile);

//about_us
$myfile = fopen("Content_product/about-us.txt", "r") or die("Unable to open file!");
$about_us =  fread($myfile,filesize("Content_product/about-us.txt"));
fclose($myfile);

?>