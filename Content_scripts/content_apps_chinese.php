<?php

//Nav - Items
$myfile = fopen("content_apps_chinese/nav-menu.txt", "r") or die("Unable to open file!");
$i=0;
$nav_items = array(5);
while(!feof($myfile)) {
    $nav_items[$i] = fgets($myfile);
    $i++;
  }
fclose($myfile);

//Language
$myfile = fopen("content_apps_chinese/language.txt", "r") or die("Unable to open file!");
$language =  fread($myfile,filesize("content_apps_chinese/language.txt"));
fclose($myfile);

//Alternative Language
$myfile = fopen("content_apps_chinese/language-alt.txt", "r") or die("Unable to open file!");
$language_alt =  fread($myfile,filesize("content_apps_chinese/language-alt.txt"));
fclose($myfile);

//Language Link
$myfile = fopen("content_apps_chinese/language-link.txt", "r") or die("Unable to open file!");
$language_link =  fread($myfile,filesize("content_apps_chinese/language-link.txt"));
fclose($myfile);

//HEADER
$myfile = fopen("content_apps_chinese/apps-header-title.txt", "r") or die("Unable to open file!");
$apps_header_title =  fread($myfile,filesize("content_apps_chinese/apps-header-title.txt"));
fclose($myfile);

//SUBTITLE
$myfile = fopen("content_apps_chinese/apps-header-subtitle.txt", "r") or die("Unable to open file!");
$apps_header_subtitle =  fread($myfile,filesize("content_apps_chinese/apps-header-subtitle.txt"));
fclose($myfile);

//Learn More Button
$myfile = fopen("content_apps_chinese/apps-header-button-title.txt", "r") or die("Unable to open file!");
$apps_header_button_title =  fread($myfile,filesize("content_apps_chinese/apps-header-button-title.txt"));
fclose($myfile);

//apps_main1_title
$myfile = fopen("content_apps_chinese/apps-main1-title.txt", "r") or die("Unable to open file!");
$apps_main1_title = fread($myfile,filesize("content_apps_chinese/apps-main1-title.txt"));
fclose($myfile);

//apps_main1_subtitle
$myfile = fopen("content_apps_chinese/apps-main1-subtitle.txt", "r") or die("Unable to open file!");
$apps_main1_subtitle =  fread($myfile,filesize("content_apps_chinese/apps-main1-subtitle.txt"));
fclose($myfile);

//apps_main1_description
$myfile = fopen("content_apps_chinese/apps-main1-description.txt", "r") or die("Unable to open file!");
$apps_main1_description =  fread($myfile,filesize("content_apps_chinese/apps-main1-description.txt"));
fclose($myfile);

//apps_main1_video
$myfile = fopen("content_apps_chinese/apps-main1-video.txt", "r") or die("Unable to open file!");
$apps_main1_video =  fread($myfile,filesize("content_apps_chinese/apps-main1-video.txt"));
fclose($myfile);

//about_us
$myfile = fopen("content_apps_chinese/about-us.txt", "r") or die("Unable to open file!");
$about_us =  fread($myfile,filesize("content_apps_chinese/about-us.txt"));
fclose($myfile);

?>