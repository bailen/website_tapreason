<?php
if(isset($_POST["olddata"]) && isset($_POST["newdata"]) && isset($_POST["filename"]))
{
$f = $_POST["filename"];
$olddata = $_POST["olddata"];
$newdata = $_POST["newdata"];
$success_msg="Updates saved successfully, please wait for a second for changes to take place.";
if(strcmp(trim($f), trim("nav-menu.txt"))==0)
{
$fp = fopen("../Content_product_chinese/".$f, "r");
$fp1= fopen("../Content_product_chinese/sample_file.txt","w");
$data='';

while(!feof($fp)) {
    
     $data = fgets($fp);
     if(strcmp(trim($data), trim($olddata))==0){
        
        fwrite($fp1,$newdata.PHP_EOL);
        
     }
     else{
         fwrite($fp1,$data);
     }
  }
  fclose($fp);
  fclose($fp1);
  unlink("../Content_product_chinese/".$f);
  rename("../Content_product_chinese/sample_file.txt", "../Content_product_chinese/nav-menu.txt");
  echo $success_msg;
}

//Language
if(strcmp(trim($f), trim("language.txt"))==0)
{
    $fp = fopen("../Content_product_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//Alternative Language
if(strcmp(trim($f), trim("language-alt.txt"))==0)
{
    $fp = fopen("../Content_product_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//Language Link
if(strcmp(trim($f), trim("language-link.txt"))==0)
{
    $fp = fopen("../Content_product_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//Header text
if(strcmp(trim($f), trim("product-header-title.txt"))==0)
{
    $fp = fopen("../Content_product_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//Subtitle
if(strcmp(trim($f), trim("product-header-subtitle.txt"))==0)
{
    $fp = fopen("../Content_product_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//product-header-button-title
if(strcmp(trim($f), trim("product-header-button-title.txt"))==0)
{
    $fp = fopen("../Content_product_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

///product-main1-title 
if(strcmp(trim($f), trim("/product-main1-title.txt"))==0)
{
    $fp = fopen("../Content_product_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//product_main1_subtitle 
if(strcmp(trim($f), trim("product_main1_subtitle.txt"))==0)
{
    $fp = fopen("../Content_product_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//product_main1_description
if(strcmp(trim($f), trim("product-main1-description.txt"))==0)
{
    $fp = fopen("../Content_product_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//product_main2_subtitle
if(strcmp(trim($f), trim("product-main2-subtitle.txt"))==0)
{
    $fp = fopen("../Content_product_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//product_main2_title
if(strcmp(trim($f), trim("product-main2-title.txt"))==0)
{
    $fp = fopen("../Content_product_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//product_main2_description
if(strcmp(trim($f), trim("product-main2-description.txt"))==0)
{
    $fp = fopen("../Content_product_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//product_main3_subtitle
if(strcmp(trim($f), trim("product-main3-subtitle.txt"))==0)
{
    $fp = fopen("../Content_product_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//product_main3_title
if(strcmp(trim($f), trim("product-main3-title.txt"))==0)
{
    $fp = fopen("../Content_product_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//product_main3_description
if(strcmp(trim($f), trim("product-main3-description.txt"))==0)
{
    $fp = fopen("../Content_product_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//about_us
if(strcmp(trim($f), trim("about-us.txt"))==0)
{
    $fp = fopen("../Content_product_chinese/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}


}
?>