<?php
if(isset($_POST["olddata"]) && isset($_POST["newdata"]) && isset($_POST["filename"]))
{
$f = $_POST["filename"];
$olddata = $_POST["olddata"];
$newdata = $_POST["newdata"];
$success_msg="Updates saved successfully, please wait for a second for changes to take place.";
if(strcmp(trim($f), trim("nav-menu.txt"))==0)
{
$fp = fopen("../Content_ecommerce/".$f, "r");
$fp1= fopen("../Content_ecommerce/sample_file.txt","w");
$data='';

while(!feof($fp)) {
    
     $data = fgets($fp);
     if(strcmp(trim($data), trim($olddata))==0){
        
        fwrite($fp1,$newdata.PHP_EOL);
        
     }
     else{
         fwrite($fp1,$data);
     }
  }
  fclose($fp);
  fclose($fp1);
  unlink("../Content_ecommerce/".$f);
  rename("../Content_ecommerce/sample_file.txt", "../Content_ecommerce/nav-menu.txt");
  echo $success_msg;
}

//Language
if(strcmp(trim($f), trim("language.txt"))==0)
{
    $fp = fopen("../Content_ecommerce/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//Alternative Language
if(strcmp(trim($f), trim("language-alt.txt"))==0)
{
    $fp = fopen("../Content_ecommerce/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//Language Link
if(strcmp(trim($f), trim("language-link.txt"))==0)
{
    $fp = fopen("../Content_ecommerce/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//Header text
if(strcmp(trim($f), trim("ecommerce-header-title.txt"))==0)
{
    $fp = fopen("../Content_ecommerce/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//Subtitle
if(strcmp(trim($f), trim("ecommerce-header-subtitle.txt"))==0)
{
    $fp = fopen("../Content_ecommerce/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//ecommerce-header-button-title
if(strcmp(trim($f), trim("ecommerce-header-button-title.txt"))==0)
{
    $fp = fopen("../Content_ecommerce/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

///ecommerce-main1-title 
if(strcmp(trim($f), trim("/ecommerce-main1-title.txt"))==0)
{
    $fp = fopen("../Content_ecommerce/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//ecommerce_main1_subtitle 
if(strcmp(trim($f), trim("ecommerce_main1_subtitle.txt"))==0)
{
    $fp = fopen("../Content_ecommerce/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//ecommerce_main1_description
if(strcmp(trim($f), trim("ecommerce-main1-description.txt"))==0)
{
    $fp = fopen("../Content_ecommerce/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//ecommerce_main1_video
if(strcmp(trim($f), trim("ecommerce-main1-video.txt"))==0)
{
    $fp = fopen("../Content_ecommerce/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//about_us
if(strcmp(trim($f), trim("about-us.txt"))==0)
{
    $fp = fopen("../Content_ecommerce/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}


}
?>