<?php
if(isset($_POST["olddata"]) && isset($_POST["newdata"]) && isset($_POST["filename"]))
{
$f = $_POST["filename"];
$olddata = $_POST["olddata"];
$newdata = $_POST["newdata"];
$success_msg="Updates saved successfully, please wait for a second for changes to take place.";
if(strcmp(trim($f), trim("nav-menu.txt"))==0)
{
$fp = fopen("../Content_company/".$f, "r");
$fp1= fopen("../Content_company/sample_file.txt","w");
$data='';

while(!feof($fp)) {
    
     $data = fgets($fp);
     if(strcmp(trim($data), trim($olddata))==0){
        
        fwrite($fp1,$newdata.PHP_EOL);
        
     }
     else{
         fwrite($fp1,$data);
     }
  }
  fclose($fp);
  fclose($fp1);
  unlink("../Content_company/".$f);
  rename("../Content_company/sample_file.txt", "../Content_company/nav-menu.txt");
  echo $success_msg;
}

//Language
if(strcmp(trim($f), trim("language.txt"))==0)
{
    $fp = fopen("../Content_company/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//Alternative Language
if(strcmp(trim($f), trim("language-alt.txt"))==0)
{
    $fp = fopen("../Content_company/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//Language Link
if(strcmp(trim($f), trim("language-link.txt"))==0)
{
    $fp = fopen("../Content_company/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//Mission Title
if(strcmp(trim($f), trim("mission-title.txt"))==0)
{
    $fp = fopen("../Content_company/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//Mission Subtitle
if(strcmp(trim($f), trim("mission-subtitle.txt"))==0)
{
    $fp = fopen("../Content_company/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//company-header-button-title
if(strcmp(trim($f), trim("company-header-button-title.txt"))==0)
{
    $fp = fopen("../Content_company/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}

//about_us
if(strcmp(trim($f), trim("about-us.txt"))==0)
{
    $fp = fopen("../Content_company/".$f,"w");
    fwrite($fp,$newdata);
    fclose($fp);
    echo $success_msg;
}


}
?>