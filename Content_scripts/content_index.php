<?php

//Nav - Items
$myfile = fopen("Content_index/nav-menu.txt", "r") or die("Unable to open file!");
$i=0;
$nav_items = array(5);
while(!feof($myfile)) {
    $nav_items[$i] = fgets($myfile);
    $i++;
  }
fclose($myfile);

//Language
$myfile = fopen("Content_index/language.txt", "r") or die("Unable to open file!");
$language =  fread($myfile,filesize("Content_index/language.txt"));
fclose($myfile);

//Alternative Language
$myfile = fopen("Content_index/language-alt.txt", "r") or die("Unable to open file!");
$language_alt =  fread($myfile,filesize("Content_index/language-alt.txt"));
fclose($myfile);

//Language Link
$myfile = fopen("Content_index/language-link.txt", "r") or die("Unable to open file!");
$language_link =  fread($myfile,filesize("Content_index/language-link.txt"));
fclose($myfile);

//HEADER
$myfile = fopen("Content_index/homePage-header-title.txt", "r") or die("Unable to open file!");
$homePage_header_title =  fread($myfile,filesize("Content_index/homePage-header-title.txt"));
fclose($myfile);

//SUBTITLE
$myfile = fopen("Content_index/homePage-header-subtitle.txt", "r") or die("Unable to open file!");
$homePage_header_subtitle =  fread($myfile,filesize("Content_index/homePage-header-subtitle.txt"));
fclose($myfile);

//Learn More Button
$myfile = fopen("Content_index/homePage-header-button-title.txt", "r") or die("Unable to open file!");
$homePage_header_button_title =  fread($myfile,filesize("Content_index/homePage-header-button-title.txt"));
fclose($myfile);

//homePage_main1_title
$myfile = fopen("Content_index/homePage-main1-title.txt", "r") or die("Unable to open file!");
$homePage_main1_title = fread($myfile,filesize("Content_index/homePage-main1-title.txt"));
fclose($myfile);

//homePage_main1_subtitle
$myfile = fopen("Content_index/homePage-main1-subtitle.txt", "r") or die("Unable to open file!");
$homePage_main1_subtitle =  fread($myfile,filesize("Content_index/homePage-main1-subtitle.txt"));
fclose($myfile);

//homePage-main1-icon1-title
$myfile = fopen("Content_index/homePage-main1-icon1-title.txt", "r") or die("Unable to open file!");
$homePage_main1_icon1_title =  fread($myfile,filesize("Content_index/homePage-main1-icon1-title.txt"));
fclose($myfile);

//homePage-main1-icon1-description
$myfile = fopen("Content_index/homePage-main1-icon1-description.txt", "r") or die("Unable to open file!");
$homePage_main1_icon1_description =  fread($myfile,filesize("Content_index/homePage-main1-icon1-description.txt"));
fclose($myfile);

//homePage_main1_icon2_title
$myfile = fopen("Content_index/homePage-main1-icon2-title.txt", "r") or die("Unable to open file!");
$homePage_main1_icon2_title =  fread($myfile,filesize("Content_index/homePage-main1-icon2-title.txt"));
fclose($myfile);

//homePage_main1_icon2_description
$myfile = fopen("Content_index/homePage-main1-icon2-description.txt", "r") or die("Unable to open file!");
$homePage_main1_icon2_description =  fread($myfile,filesize("Content_index/homePage-main1-icon2-description.txt"));
fclose($myfile);

//homePage_main1_icon3_title
$myfile = fopen("Content_index/homePage-main1-icon3-title.txt", "r") or die("Unable to open file!");
$homePage_main1_icon3_title =  fread($myfile,filesize("Content_index/homePage-main1-icon3-title.txt"));
fclose($myfile);

//homePage_main1_icon3_description
$myfile = fopen("Content_index/homePage-main1-icon3-description.txt", "r") or die("Unable to open file!");
$homePage_main1_icon3_description =  fread($myfile,filesize("Content_index/homePage-main1-icon3-description.txt"));
fclose($myfile);

//homePage_main2_subtitle
$myfile = fopen("Content_index/homePage-main2-subtitle.txt", "r") or die("Unable to open file!");
$homePage_main2_subtitle =  fread($myfile,filesize("Content_index/homePage-main2-subtitle.txt"));
fclose($myfile);

//homePage_main2_title
$myfile = fopen("Content_index/homePage-main2-title.txt", "r") or die("Unable to open file!");
$homePage_main2_title =  fread($myfile,filesize("Content_index/homePage-main2-title.txt"));
fclose($myfile);

//homePage_main2_description
$myfile = fopen("Content_index/homePage-main2-description.txt", "r") or die("Unable to open file!");
$homePage_main2_description =  fread($myfile,filesize("Content_index/homePage-main2-description.txt"));
fclose($myfile);

//homePage_main2_button_title
$myfile = fopen("Content_index/homePage-main2-button-title.txt", "r") or die("Unable to open file!");
$homePage_main2_button_title =  fread($myfile,filesize("Content_index/homePage-main2-button-title.txt"));
fclose($myfile);

//homePage_main3_subtitle
$myfile = fopen("Content_index/homePage-main3-subtitle.txt", "r") or die("Unable to open file!");
$homePage_main3_subtitle =  fread($myfile,filesize("Content_index/homePage-main3-subtitle.txt"));
fclose($myfile);

//homePage_main3_title
$myfile = fopen("Content_index/homePage-main3-title.txt", "r") or die("Unable to open file!");
$homePage_main3_title =  fread($myfile,filesize("Content_index/homePage-main3-title.txt"));
fclose($myfile);

//homePage_main3_description
$myfile = fopen("Content_index/homePage-main3-description.txt", "r") or die("Unable to open file!");
$homePage_main3_description =  fread($myfile,filesize("Content_index/homePage-main3-description.txt"));
fclose($myfile);

//homePage_main3_button_title
$myfile = fopen("Content_index/homePage-main3-button-title.txt", "r") or die("Unable to open file!");
$homePage_main3_button_title =  fread($myfile,filesize("Content_index/homePage-main3-button-title.txt"));
fclose($myfile);

//homePage_main4_subtitle
$myfile = fopen("Content_index/homePage-main4-subtitle.txt", "r") or die("Unable to open file!");
$homePage_main4_subtitle =  fread($myfile,filesize("Content_index/homePage-main4-subtitle.txt"));
fclose($myfile);

//homePage_main4_title
$myfile = fopen("Content_index/homePage-main4-title.txt", "r") or die("Unable to open file!");
$homePage_main4_title =  fread($myfile,filesize("Content_index/homePage-main4-title.txt"));
fclose($myfile);

//homePage_main4_description
$myfile = fopen("Content_index/homePage-main4-description.txt", "r") or die("Unable to open file!");
$homePage_main4_description =  fread($myfile,filesize("Content_index/homePage-main4-description.txt"));
fclose($myfile);

//homePage_main4_button_title
$myfile = fopen("Content_index/homePage-main4-button-title.txt", "r") or die("Unable to open file!");
$homePage_main4_button_title =  fread($myfile,filesize("Content_index/homePage-main4-button-title.txt"));
fclose($myfile);

//about_us
$myfile = fopen("Content_index/about-us.txt", "r") or die("Unable to open file!");
$about_us =  fread($myfile,filesize("Content_index/about-us.txt"));
fclose($myfile);

?>