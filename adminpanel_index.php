<?php
session_start();
include "Content_scripts/content_index.php";
if(isset($_SESSION["admin"]))
{
?>
<html>
<head>
    <title>Admin Panel</title>
    <meta charset="utf-8">
    <!-- Latest compiled and minified CSS -->
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
 
 <!-- jQuery library -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 
 <!-- Popper JS -->
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
 
 <!-- Latest compiled JavaScript -->
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> 

 <!-- Font Awesome CDN -->
 <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" rel="stylesheet">
</head>
<body>

<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="#">Tap Reason Admin Dashboard</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="#" class="btn btn-danger text-light" id="logout">Logout <i class="fa fa-power-off"></i></a>
      </li>    
    </ul> 
</nav>
<div class="container-fluid">
    <div class="alert"></div>
    <div><a href="adminpanel.php"><i class="fa fa-arrow-circle-left"></i> Go back to Adminpanel </a></div><br/>
<table class="table table-responsive-sm">
<tr>
    <th>Name of the element</th>
    <th>Value</th>
    <th>Action</th>
</tr>
<tr>
<td>Navigation Item 1</td>
<td><p><?php echo $nav_items[0]; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="nav-menu.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>Navigation Item 2</td>
<td><p><?php echo $nav_items[1]; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="nav-menu.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>Navigation Item 3</td>
<td><p><?php echo $nav_items[2]; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="nav-menu.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>Navigation Item 4</td>
<td><p><?php echo $nav_items[3]; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="nav-menu.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>language</td>
<td><p><?php echo $language; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="language.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>language_alt</td>
<td><p><?php echo $language_alt; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="language-alt.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>language_link</td>
<td><p><?php echo $language_link; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="language-link.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_header_title</td>
<td><p><?php echo $homePage_header_title; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-header-title.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_header_subtitle</td>
<td><p><?php echo $homePage_header_subtitle; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-header-subtitle.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_header_button_title</td>
<td><p><?php echo $homePage_header_button_title; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-header-button-title.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main1_title</td>
<td><p><?php echo $homePage_main1_title; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main1-title.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main1_subtitle</td>
<td><p><?php echo $homePage_main1_subtitle; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main1-subtitle.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main1_icon1_title</td>
<td><p><?php echo $homePage_main1_icon1_title; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main1-icon1-title.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main1_icon1_description</td>
<td><p><?php echo $homePage_main1_icon1_description; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main1-icon1-description.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main1_icon2_title</td>
<td><p><?php echo $homePage_main1_icon2_title; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main1-icon2-title.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main1_icon2_description</td>
<td><p><?php echo $homePage_main1_icon2_description; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main1-icon2-description.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main1_icon3_title</td>
<td><p><?php echo $homePage_main1_icon3_title; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main1-icon3-title.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main1_icon3_description</td>
<td><p><?php echo $homePage_main1_icon3_description; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main1-icon3-description.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main2_subtitle</td>
<td><p><?php echo $homePage_main2_subtitle; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main2-subtitle.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main2_title</td>
<td><p><?php echo $homePage_main2_title; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main2-title.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main2_description</td>
<td><p><?php echo $homePage_main2_description; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main2-description.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main2_button_title</td>
<td><p><?php echo $homePage_main2_button_title; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main2-button-title.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main3_subtitle</td>
<td><p><?php echo $homePage_main3_subtitle; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main3-subtitle.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main3_title</td>
<td><p><?php echo $homePage_main3_title; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main3-title.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main3_description</td>
<td><p><?php echo $homePage_main3_description; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main3-description.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main3_button_title</td>
<td><p><?php echo $homePage_main3_button_title; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main3-button-title.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main4_subtitle</td>
<td><p><?php echo $homePage_main4_subtitle; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main4-subtitle.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main4_title</td>
<td><p><?php echo $homePage_main4_title; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main4-title.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main4_description</td>
<td><p><?php echo $homePage_main4_description; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main4-description.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>homePage_main4_button_title</td>
<td><p><?php echo $homePage_main4_button_title; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="homePage-main4-button-title.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<tr>
<td>about_us</td>
<td><p><?php echo $about_us; ?></p></td>
<td>
<textarea class="new-text" rows="10" cols="40" placeholder="New text here ..."></textarea>
<a href="#" class="btn btn-primary edit">Edit <i class="fa fa-edit"></i></a>
<a href="#" class="btn btn-success save" fileassoc="about-us.txt">Save <i class="fa fa-save"></i></a>
<a href="#" class="btn btn-danger cancel">Cancel <i class="fa fa-close"></i></a>
</td>
</tr>

<!--Logout script-->
<script>
    $(document).ready(function(){
        $("#logout").click(function(){
            window.location.assign("logout.php");
        });
    });
    </script>

    <!--Script to edit the items-->
    <script>
        $(document).ready(function(){
            $(".save").hide();
            $(".cancel").hide();
            $(".new-text").hide();
            $(".edit").click(function(e){
                e.preventDefault();
                $(this).siblings(".save").show();
                $(this).siblings(".cancel").show();
                $(this).addClass("disabled");
                $(this).siblings(".new-text").show();
                
           
            });
        });
    </script>

    <!--Script to cancel editing-->
    <script>
        $(document).ready(function(){
            $(".cancel").click(function(e){
                e.preventDefault();
               $(this).siblings(".new-text").hide();
               $(".edit").removeClass("disabled");
               $(this).siblings(".save").hide();
               $(this).hide();
            });
        });
    </script>
    <!-- Saving the new data -->
    <script>
        $(document).ready(function(){
            $(".save").click(function(){
                var olddata=$(this).parent().prev().text();
                var newdata = $(this).siblings(".new-text").val();
                $.post("Content_scripts/change_content_index.php", {olddata:olddata, newdata:newdata, filename:$(this).attr("fileassoc")}, function(data, status){
                    $(".alert").text(data).addClass("alert-success");
                });
                setTimeout(function(){
                    window.location.reload();
                }, 1500)
            });
        });
    </script>

</body>
</html>
<?php
}
else{
    header('location:index.php');
}
?>