<?php
if(isset($_GET["lang"]))
{
    if($_GET["lang"]==="ch")
    {
        include "Web_content_scripts/content_chinese.php";
    }
    else{
        include "Web_content_scripts/content.php";        
    }
}
else{
include "Web_content_scripts/content.php";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="keywords" content="Artificial intelligence, ">
        <title>Website TapReason</title>    
        <!-- Plugins CSS -->
        <link href="css/plugins/plugins.css" rel="stylesheet">
        <link href="linearicons/fonts.css" rel="stylesheet">
        <link href="css/style-saas-landing.css" rel="stylesheet">
        <link href="css/home.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
    </head>

<body data-spy="scroll" data-target="#landingkit-navbar" data-offset="75">
        <div id="preloader">
            <div id="preloader-inner"></div>
        </div><!--/preloader-->

        <nav class="navbar navbar-expand-lg navbar-light bg-white navbar-transparent-light navbar-sticky">
            <div class="container">
                <a class="navbar-brand" href="./home.html">
                    <img src="images/logo3.png" alt="" class='logo-scroll'>
                    <img src="images/logo1.png" alt="" class='logo-trans'>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#landingkit-navbar" aria-controls="landingkit-navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="landingkit-navbar">
                    <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                    <span class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <a  data-scroll ><?php echo $nav_items[0]; ?></a>
                                    </span>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="./product.html">Underlying magic</a>
                                        <a class="dropdown-item" href="./product.html">Moment Scoring in Real-Time</a>
                                        <a class="dropdown-item" href="./product.html">Dynamic Moment Scoring</a>
                                    </div>
                            </li>

                            <li class="nav-item dropdown">
                                    <span class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <a  data-scroll ><?php echo $nav_items[1]; ?></a>
                                    </span>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="./e-commerce.html">e-Commerce Solutions</a>
                                        <a class="dropdown-item" href="./apps.html">Apps Solutions</a>
                                        <a class="dropdown-item" href="./travel.html">Travel Solutions</a>
                                    </div>
                            </li> 
                            
                            <li class="nav-item dropdown">
                                    <span class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <a  data-scroll ><?php echo $nav_items[2]; ?></a>
                                    </span>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="./company.html">Our mission</a>
                                        <a class="dropdown-item" href="./company.html">Our team</a>
                                        <a class="dropdown-item" href="./company.html">Awards and certifications</a>
                                        
                                    </div>
                            </li>   

                            <li class="nav-item dropdown">
                                    <span class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <a  data-scroll ><?php echo $nav_items[3]; ?></a>
                                    </span>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="./resources.html"></a>
                                        <a class="dropdown-item" href="./resources.html"></a>
                                        <a class="dropdown-item" href="./resources.html"></a>
                                        <a class="dropdown-item" href="./resources.html"></a>
                                    </div>
                            </li>  
                                 
                        <li class="nav-item dropdown">
                            <span class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                EN
                            </span>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="index.php?lang=ch">Chinese</a>
                                <a class="dropdown-item" href="index.php">English</a>
                                
                                
                            </div>
                        </li>
                        <li class="nav-item">
                                <a href="https://wrapbootstrap.com/theme/assan-multipurpose-18-themes-admin-WB05F069P/?ref=wb_rakesh" target="_blank" class="btn btn-white-outline modal-video my-3 btn-gray">SCHEDULE DEMO</a> 
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="home" class="fullScreen-particles bg-primary-dark background-color-home">
          
          <div class="content-table">
             <div class="content-middle">
         <div class="container">
             <div class="row align-items-center">
                 <div class="col-lg-8 text-center mr-auto ml-auto">
                     <h3 class="mb20 h1 font300 pt100 text-white"><?php echo $homePage_header_title;?></h3>
                     <p class=" mb30 lead text-white-gray">
                     <?php echo $homePage_header_subtitle;?>
                     </p>
                     <div>
                         <a data-scroll href="#features" class="btn btn-primary mr-3 my-3"><?php echo $homePage_header_button_title;?></a>
                         
                      
                     </div>
                 </div>
             </div>
         </div>
             </div>
          </div>
     </div><!--hero-->

     <main class="main-content">
         <section id="features" class='gradient-faded'>
             <div class="container pt90 pb90">
                 <h6 class="text-center section-subtitle"><?php echo $homePage_main1_subtitle;?></h6>
                 <h3 class="text-center section-title mb70"><?php echo $homePage_main1_title;?></h3>
                 <div class="row">
                     <div class="col-lg-4 border border-faded border-top-0 border-left-0 feature-icon text-center">
                         <i class="icon-magnifier text-primary"></i>
                         <h4><?php echo $homePage_main1_icon1_title;?></h4>
                         <p>
                         <?php echo $homePage_main1_icon1_description;?>
                         </p>
                     </div>
                     <div class="col-lg-4 feature-icon border border-faded border-left-0 border-top-0 text-center">
                         <i class="icon-cable text-success"></i>
                         <h4><?php echo $homePage_main1_icon2_title;?></h4>
                         <p>
                         <?php echo $homePage_main1_icon2_description;?>
                         </p>
                     </div>
                     <div class="col-lg-4 feature-icon border border-faded border-left-0 border-top-0 border-right-0 text-center">
                         <i class="icon-coin-dollar text-danger"></i>
                         <h4><?php echo $homePage_main1_icon3_title;?></h4>
                         <p>
                         <?php echo $homePage_main1_icon3_description;?>
                         </p>
                     </div>
                 </div>
             </div>
         </section><!--features-->
         <div class="bg-primary-dark feature-img-section pt90 pb90">
             <div class="container">
                 <div class="row">
                     <div class="col-lg-6">
                         <h6 class='section-subtitle text-white'><?php echo $homePage_main2_subtitle;?></h6>
                         <h2 class='text-white'><?php echo $homePage_main2_title;?></h2>
                         <p class="text-white-gray">
                         <?php echo $homePage_main2_description;?>
                         </p>
                         
                     </div>
                     <div class="col-lg-6 center">
                             <div id="user-value">
                                     <div class='icon-container' id='icon-top-left'>
                                         <i class="icon-register"></i>
                                         <span>Register</span>
                                     </div>
                                     <div class='icon-container' id='icon-top-right'>
                                         <i class="icon-cash-dollar"></i>
                                         <span>Buy</span>
                                     </div>
                                     <div class='icon-container' id='icon-left-top'>
                                         <i class="icon-cloud-download"></i>
                                         <span>Download</span>
                                     </div>
                                     <div class='icon-container' id='icon-left-bottom'>
                                         <i class="icon-bullhorn"></i>
                                         <span>Share</span>
                                     </div>
                                     <div class='icon-container' id='icon-bottom-left'>
                                         <i class="icon-envelope"></i>
                                         <span>Suscribe</span>
                                     </div>
                                     <div class='icon-container' id='icon-bottom-right'>
                                         <i class="icon-star"></i>
                                         <span>Review</span>
                                     </div>
                                     <div class='icon-container' id='icon-right-top'>
                                         <i class="icon-bubble-dots"></i>
                                         <span>Comment</span>
                                     </div>
                                     <div class='icon-container' id='icon-right-bottom'>
                                         <i class="icon-tag"></i>
                                         <span>Reedem Coupon</span>
                                     </div>
                                 </div>
                     </div>

                 </div>
             </div>
         </div><!--feature-img-->
         <div class="bg-white pt90 pb50">
             <div class="container full-width">
                 <div class="row align-items-center">
                     <div class='col-lg-6 pb40 center'>
                         <div id="user-value-text">
                                 <div class='icon-container' id='icon-text-top-left'>
                                     <i class="icon-circle"></i>
                                     <span class="number">6.9%</span>
                                     <span>Register</span>
                                 </div>
                                 <div class='icon-container' id='icon-text-top-right'>
                                     <i class="icon-circle"></i>
                                     <span class="number">0.7%</span>
                                     <span>Buy</span>
                                 </div>
                                 <div class='icon-container' id='icon-text-left-top'>
                                     <i class="icon-circle"></i>
                                     <span class="number">5.4%</span>
                                     <span>Download</span>
                                 </div>
                                 <div class='icon-container' id='icon-text-left-bottom'>
                                     <i class="icon-circle"></i>
                                     <span class="number">3.7%</span>
                                     <span>Share</span>
                                 </div>
                                 <div class='icon-container' id='icon-text-bottom-left'>
                                     <i class="icon-circle"></i>
                                     <span class="number">6.2%</span>
                                     <span>Subscribe</span>
                                 </div>
                                 <div class='icon-container' id='icon-text-bottom-right'>
                                     <i class="icon-circle"></i>
                                     <span class="number">11%</span>
                                     <span>Review</span>
                                 </div>
                                 <div class='icon-container' id='icon-text-right-top'>
                                     <i class="icon-circle"></i>
                                     <span class="number">9.3%</span>
                                     <span>Comment</span>
                                 </div> 
                                 <div class='icon-container' id='icon-text-right-bottom'>
                                     <i class="icon-circle"></i>
                                     <span class="number">16%</span>
                                     <span>Reedem Coupon</span>
                                 </div>
                             </div>
                     </div>
                     <div class="col-lg-5 ml-auto pb40">
                         <h6 class='section-subtitle'><?php echo $homePage_main3_subtitle;?></h6>
                         <h2><?php echo $homePage_main3_title;?></h2>
                         <p>
                         <?php echo $homePage_main3_description;?>
                         </p>
                         <a href='#' class='btn btn-outline-primary btn-rounded'><?php echo $homePage_main3_button_title;?></a>
                     </div>

                 </div>
             </div>
         </div><!--feature-img-->
         <div class="bg-faded pt90 pb50">
             <div class="container">
                 <div class="row align-items-center">

                     <div class="col-lg-5 pb40">
                         <h6 class='section-subtitle'><?php echo $homePage_main4_subtitle;?></h6>
                         <h2><?php echo $homePage_main4_title;?></h2>
                         <p>
                         <?php echo $homePage_main4_description;?>
                         </p>
                         <a href='#' class='btn btn-outline-primary btn-rounded'><?php echo $homePage_main4_button_title;?></a>
                     </div>
                     <div class='col-lg-6 ml-auto pb40 center'>
                             <div id="user-value-two">
                                     <div class='icon-container' id='icon-text-top-left'>
                                         <i class="icon-circle"></i>
                                         <span class="number">6.9%</span>
                                         <span>Register</span>
                                     </div>
                                     <div class='icon-container' id='icon-text-top-right'>
                                         <i class="icon-circle"></i>
                                         <span class="number">0.7%</span>
                                         <span>Buy</span>
                                     </div>
                                     <div class='icon-container' id='icon-text-left-top'>
                                         <i class="icon-circle"></i>
                                         <span class="number">5.4%</span>
                                         <span>Download</span>
                                     </div>
                                     <div class='icon-container' id='icon-text-left-bottom'>
                                         <i class="icon-circle"></i>
                                         <span class="number">3.7%</span>
                                         <span>Share</span>
                                     </div>
                                     <div class='icon-container' id='icon-text-bottom-left'>
                                         <i class="icon-circle"></i>
                                         <span class="number">6.2%</span>
                                         <span>Subscribe</span>
                                     </div>
                                     <div class='icon-container' id='icon-text-bottom-right'>
                                         <i class="icon-circle"></i>
                                         <span class="number">11%</span>
                                         <span>Review</span>
                                     </div>
                                     <div class='icon-container' id='icon-text-right-top'>
                                         <i class="icon-circle"></i>
                                         <span class="number">9.3%</span>
                                         <span>Comment</span>
                                     </div> 
                                     <div class='icon-container' id='icon-text-right-bottom'>
                                         <i class="icon-circle"></i>
                                         <span class="number">16%</span>
                                         <span>Reedem Coupon</span>
                                     </div>
                                 </div>
                     </div>
                 </div>
             </div>
         </div><!--feature-img-->
         <section id='reviews' class='pt90 pb90'>
                 <div class="container">
 
                     <h4 class="text-dark-gray text-center">Trusted by landing companies</h4>
                     
 
                     <div class="row wow fadeInDown" data-wow-delay=".1s">
                         <div class="col owl-carousel owl-clients owl-theme">
                             <div class="item">
                                 <img src="images/adeco-logo.png" alt="">
                             </div>
                             <div class="item">
                                 <img src="images/dangdang-com-logo.png" alt="">
                             </div>
                             <div class="item">
                                 <img src="images/intuit-logo.png" alt="">
                             </div>
                             <div class="item">
                                 <img src="images/isracard-logo.png" alt="">
                             </div>
                             <div class="item">
                                 <img src="images/Lifesum-logo.png" alt="">
                             </div>
                             <div class="item">
                                 <img src="images/medisafe-logo.png" alt="">
                             </div>
                             <div class="item">
                                 <img src="images/prosperdaily-logo.png" alt="">
                             </div>
                             <div class="item vip-com-logo">
                                 <img src="images/vip-com-logo.png" alt="">
                             </div>
                             <div class="item zap-logo">
                                 <img src="images/zap-logo.jpg" alt="">
                             </div>
                             <div class="item">
                                 <img src="images/intuit-logo.png" alt="">
                             </div>
                         </div>
                     </div>
                 </div>
             </section>


     </main><!--main-content-->
     <div  class="bg-parallax pt100 pb100" data-jarallax='{"speed": 0.2}' style='background-image: url("images/bg2.jpg")'>
         <div class='parallax-overlay'></div>
         <div class="container">
             <div class="row">
                  <div class="col-md-8 mr-auto ml-auto text-center pt100 pb100">
                      <h2 class="text-white font300 h1 mb40">You're in good company</h2>
                      <a href='#' class='btn btn-lg btn-primary'>SCHEDULE DEMO</a>
             </div>
             </div>
         </div>
     </div>  
        
     <footer class="footer">
         <div class="container">
             <div class="row">
                 <div class="social-buttons col-lg-3 mb30">
                     <h5 class=" mb20">Follow Us</h5>
                     <span>
                         <a href="#" class="social-icon si-dark si-facebook si-dark-round">
                             <i class="fa fa-facebook"></i>
                             <i class="fa fa-facebook"></i>
                         </a>
                         <a href="#" class="social-icon si-dark si-twitter si-dark-round">
                             <i class="fa fa-twitter"></i>
                             <i class="fa fa-twitter"></i>
                         </a>
                         <a href="#" class="social-icon si-dark si-g-plus si-dark-round">
                             <i class="fa fa-google-plus"></i>
                             <i class="fa fa-google-plus"></i>
                         </a>
                         <a href="#" class="social-icon si-dark si-skype si-dark-round">
                             <i class="fa fa-skype"></i>
                             <i class="fa fa-skype"></i>
                         </a>
                     </span>
                 </div>
                 <div class='col-lg-3'>
                     <h5 class=" mb20">Call Us</h5> 
                     <p class='lead'>+01 123-4567-089</p>
                 </div>
                  <div class='col-lg-3'>
                     <h5 class=" mb20">Mail Us</h5> 
                     <p class='lead'>hello [at] tapreason.com</p>
                 </div>
                  <div class='col-lg-3'>
                     <h5 class=" mb20">Request your</h5> 
                     <a class='btn btn-primary'>DEMO</a>
                 </div>
             </div><hr class="mb70">
             <div class="row align-items-start">
                 <div class="col-lg-5 mb30">
                     <h5 class="mb20">About us</h5>
                     <ul class="list-unstyled">
                         <li>
                             <a href="#">TapReason is an award winning, artificial intelligence based, positive-moment scoring system.

                                 We capture tens of millions of positive-moments a day, optimizing conversion rates on different platforms.
                             </a>
                         </li>
                     </ul>
                    
                         <h5 class="mb20 title-offices">Offices</h5>
                         <ul class="list-unstyled">
                         <li>
                             <a href="#">US: 85 Broad St. New York, NY 10004.</a>
                         </li>
                         <li>
                             <a href="#">CN: 588 Yan'an East Road Huangpu District Shanghai 31.</a>
                         </li>
                         <li>
                             <a href="#">IL: HaMada 1 St, Rehovot.</a>
                         </li>
                         <li>
                             <a href="#"></a> 
                         </li>
                     </ul>
                 </div>
                 <div class="col-lg-3 ml-auto mb30">
                     <h5 class="mb20">Product</h5>
                     <ul class="list-unstyled">
                         <li>
                             <a href="./product.html">Underlying Magic</a>
                         </li>
                         <li>
                             <a href="./customers.html">Moment Scoring in Real-Time</a>
                         </li>
                         <li>
                             <a href="./company.html">Dynamic Moment Scoring</a>
                         </li>
                     </ul>
                     </br>
                         <h5 class="mb20">Company</h5>
                     <ul class="list-unstyled">
                         <li>
                             <a href="#">Our mission</a>
                         </li>
                         <li>
                             <a href="#">Our team</a>
                         </li>
                         <li>
                             <a href="#">Awards and certifications</a>
                         </li>
                         
                     </ul>
                 </div>
                 <div class="col-lg-3 mr-auto mb30">
                     <h5 class="mb20">Customers</h5>
                     <ul class="list-unstyled">
                         <li>
                             <a href="#">E-Commerce Solutions</a>
                         </li>
                         <li>
                             <a href="#">Apps Solutions</a>
                         </li>
                         <li>
                             <a href="#">Travel Solutions</a>
                         </li>
                     </ul>     
                     </br>
                     <h5 class="mb20">Contact us</h5>
                 <ul class="list-unstyled">
                     <li>
                         <a href="#">Call us: +01123-4567-089</a>
                     </li>
                     <li>
                         <a href="#">Mail us: hello [at] tapreason.com </a>
                     </li>
                     <li>
                         <a href="#"></a>
                     </li>
                     
                 </ul>                          
                     
                 </div>
                 
             </div><hr class="mb60">
             <div class="text-center copyright">
                 <span>&copy; Copyright 2018. <a href="./home.html">TapReason</a> All rigth reserved</span>
             </div>
         </div>
     </footer>
     <!--back to top-->
     <a href="#" class="back-to-top" id="back-to-top"><i class="icon-chevron-up"></i></a>
     <!-- jQuery first, then Tether, then Bootstrap JS. -->
     <script type="text/javascript" src="js/plugins/plugins.js"></script>
     <!--tweet-scroller-->
     <script src="tweetie/tweetie.min.js" type="text/javascript"></script>
     <script type="text/javascript" src="js/saas.products.custom.js"></script>
     <script src="js/jquery.particleground.min.js"></script>
     <script type="text/javascript" src="js/landing.custom.js"></script> 
     <script>
         //particle
 $('#home').particleground({
     dotColor: '#65749f',
     lineColor: '#65749f'
 });
         </script>
 </body>
</html>

    
        